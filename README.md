### Today I Learned

1. 혼자서 사용했던 방법이 아닌 협업할 때 사용하는 방식으로 습관 만들기
2. commit convention 을 지키기
3. 파일 수정이력을 잘 파악할 수 있도록 하기
